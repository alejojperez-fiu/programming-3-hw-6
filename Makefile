#=============================================================================
#   Source code:  Makefile
#        Author:  Alejandro Perez
#    Student ID:  6088462
#    Assignment:  Program #6 Assignment 6
#=============================================================================

# The compiler: gcc for C program
CC = gcc

# the build target executable:
TARGET = SharedProcess.out

build:
	$(CC) SharedProcess.c -o $(TARGET)

clean:
	rm $(TARGET)
