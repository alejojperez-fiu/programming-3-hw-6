/*=============================================================================
|   Source code:  CommonFunctionalities.h
|        Author:  Alejandro Perez
|    Student ID:  6088462
|    Assignment:  Program #4 Assignment 4
|*===========================================================================*/

#ifndef CommonFunctionalities_H

/*=============================================================================
| INCLUDES ====================================================================
|*===========================================================================*/
/**
 * <stdio.h> It is being used in order to output information to the end user
 * <sys/types.h>, <sys/ipc.h>, and <sys/shm.h> Are being used for requesting
 *                                             shared memory
 * <unistd.h> It is used to fork child processes
 * <stdlib.h> It is used to exit from the child processes
 * <sys/wait.h> It is used to wait for child processes
 * <ctype.h> It is used to detect if a character is a digit
 */
#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <ctype.h>

/*=============================================================================
| CONSTANTS ===================================================================
|*===========================================================================*/

/**
 * Helper define to avoid including this header file twice in the program
 */
#define CommonFunctionalities_H

// PROGRAM GROUP
#define CHILD_INDEX_TO_ID_OFFSET 1 // Represents the offset used to get the
                                   // child's process id from the child's index
#define CHILD_ID_TO_INDEX_OFFSET -1 // Represents the offset used to get the
                                    // child's process index from the child's id
#define CHILD_ID_MEMORY_OFFSET -1 // Represents the offset used to get the
                                  // child's mailbox index from the child's id
#define END_OF_STRING '\0' // Represents the end of string character
#define ERROR 1
#define FORK_CHILD 0 // Result of fork in the child process
#define START_OF_SHARED_MEMORY 0 // The index where the shared memory starts
#define SUCCESS 0

// VALIDATION GROUP
#define MIN_CONSOLE_ARGUMENTS 1 // Minmum valid amount of console arguments
#define MAX_CONSOLE_ARGUMENTS 7 // Maximum valid amount of console arguments
#define MIN_VALUE 0 // Minmum valid value
#define MAX_VALUE 9 // Maximum valid value
#define NUMBER_SCANNED_CORRECTLY 1 // Used to determine if a number was scanned
                                   // correctly

/*=============================================================================
| CUSTOM TYPES ================================================================
|*===========================================================================*/

/**
 * Replicate the Boolean functionality from other programming languages
 */
typedef enum { 
    false, 
    true 
} bool;

/**
 * Represents the result PID and status of a child process that finished
 */
struct ChildProcessResult {
    pid_t PID;
    int status;
};


/*=============================================================================
| FUNCTIONS ===================================================================
|*===========================================================================*/

bool AttachShredMemory(int, int**);
void ChildProcess(const int, int*);
void CollectChildProcessesResult(int, struct ChildProcessResult[]);
void DetachShredMemory(int*);
bool ExtractIntegersFromConsoleString(int, char**, int[]);
bool ForkChildProcesses(int, int*);
int GetRealConsoleArgumentsCount(int);
bool GetSharedMemory(int, int*, int**);
bool GetSharedMemoryId(int, int*);
void InitializeIntegerArray(int, int[]);
void InitializeSharedMemory(const int, int[], const int[]);
void PrintChildProcessesResults(int, struct ChildProcessResult[]);
void PrintSharedMemory(char[], int, int, int*);
void RemoveSharedMemory(int);
bool StringIsNumber(const char[]);
bool ValidConsoleArgumentsCount(int);
bool ValidConsoleValues(int, char**, int[]);
bool ValidIntegerValuesInArray(int, int[]);

#endif