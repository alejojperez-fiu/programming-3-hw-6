/*=============================================================================
|   Source code:  SharedProcess.c
|        Author:  Alejandro Perez
|    Student ID:  6088462
|    Assignment:  Program #6 Assignment 6
|
|        Course:  COP 4338
|       Section:  U03
|    Instructor:  William Feild
|      Due Date:  November 29, 2018, at the beginning of class
|
|	I hereby certify that this collective work is my own
|	and none of it is the work of any other person or entity.
|
|	___Alejandro Perez_________________________ [Signature]
|
|      Language:  C
|   Compile/Run:  Compile SharedProcess.c. Make sure that the header file 
|                 CommonFunctionalities.h is in the same path where your are 
|                 compiling the source code.
|
|   Compile:
| 	Option 1: make
|   Option 2: make build
|   Option 3: gcc SharedProcess.c -o SharedProcess.out
|
|   Run:
|   ./SharedProcess.out 1 2 3 4 5 6 7
|       // The console arguments must be between 1 and 7 (inclusive) – each a 
|       // unique integer between 0-9 (inclusive). The amount of arguments is
|       // the amount of child processes.
| +-----------------------------------------------------------------------------
|
| Description: Based on the console input, the program will request shared
|              memory from the system, it will attach the shared memory to its 
|              own address space and fill the space (using an array) with the
|              integers from the command-line. The parent will display the 
|              initial state of the shared memory and then spawn a number of 
|              child processes equal to the number of command-line arguments,
|              all at once - not one at a time, and then wait for each and every
|              child to complete its tasks. Upon completion of all children, the
|              parent will acknowledge said completion by storage and display of
|              their respective PID & exit codes, display the final state of the
|              shared memory, detach the shared memory, remove the shared 
|              memory, and then terminate. Each child process will receive a 
|              unique ID upon initiation – an integer between 1 and the number
|              of command-line arguments (maximum 7).
|
|              Each child will display the initial state of the shared memory 
|              and their private memory - their unique ID. Next each child will
|              locate the array element (index) associated with their unique ID
|              and then multiply that array element by their unique ID, storing
|              the result in the same array element. Each child will then
|              display the current state of the shared memory, provide their
|              exit PID & status/code, and exit.
|
|       Input: Console Arguments
|               - A list of inetegers between 1 and 7 (inclusive) – each a 
|                 unique integer between 0-9 (inclusive)
|
|      Output: Each step (function?) that a process (parent or child) takes 
|              (starting, validating, requesting/removing shared memory,
|              attaching/detaching shared memory, forking/completing child
|              processes, exiting, etc.) will output a trace statement in order
|              to show which statements are currently being executed. The parent
|              will prefix each trace statement with ‘Parent: ’ and each child
|              will prefix their trace statements with a tab (or spaces) and
|              then ‘Child ID: ’, where ID is their unique ID, so that the child
|              output stands out from the parent output. Consider buffered
|              versus un-buffered output.
|
|     Process: Nothing major to highlight 
|
|   Required Features Not Included: N/A
|
|   Known Bugs:  N/A
|*===========================================================================*/

/**
 * "CommonFunctionalities.h" It is being used to group all common functionality
 *                           among the source code files
 */
#include "CommonFunctionalities.h"

/**
 * Program entry point
 *
 * @return Program execution status. If different
 * from SUCCESS then something was not correct.
 */
int main(int argc, char **args)
{
    puts("Parent: starts");

    int realArgc = GetRealConsoleArgumentsCount(argc);

    int numbers[realArgc];
    InitializeIntegerArray(realArgc, numbers);

    puts("Parent: validate command line");
    if(ValidConsoleValues(realArgc, args, numbers) == false)
    {
        return ERROR;
    }

    const int numbersSize = realArgc;    
    int sharedMemoryId = 0;
    int *sharedMemory = NULL;

    puts("Parent: requests shared memory");
    GetSharedMemory(numbersSize, &sharedMemoryId, &sharedMemory);

    puts("Parent: fills shared memory");
    InitializeSharedMemory(numbersSize, sharedMemory, numbers);

    puts("Parent: displays shared memory");
    PrintSharedMemory("    ", START_OF_SHARED_MEMORY, numbersSize, sharedMemory);

    puts("Parent: forks (each) child process");
    if(ForkChildProcesses(numbersSize, sharedMemory) == false)
    {
        return ERROR;
    }

    struct ChildProcessResult results[numbersSize];

    printf("%s\n\n", "Parent: waits for (each) child");
    CollectChildProcessesResult(numbersSize, results);
    printf("\n%s\n", "Parent: completion by storage");

    puts("Parent: displays (each) child PID & exit code");
    PrintChildProcessesResults(numbersSize, results);

    puts("Parent: displays shared memory");
    PrintSharedMemory("    ", START_OF_SHARED_MEMORY, numbersSize, sharedMemory);

    puts("Parent: detaches shared memory");
    DetachShredMemory(sharedMemory);

    puts("Parent: removes shared memory");
    RemoveSharedMemory(sharedMemoryId);

    return SUCCESS;
}

/**
 * Helper function to attach shared memory from an id provided
 * 
 * @param memoryId The memory id previously requested for the shared memory
 * @param sharedMemory The pointer to where the shared memory starts
 * 
 * @return Whether the shared memory was attached or not
 */
bool AttachShredMemory(int memoryId, int **sharedMemory)
{
    *sharedMemory = (int *) shmat(memoryId, NULL, 0);

    if ((long)*(sharedMemory) == -1)
    {
        printf(
            "%s",
            "We could not attached the shared memory.\n\n"
            "Gracefully shutting down the program.\n"
        );
        
        return false;
    }

    return true;
}

/**
 * Helper function executed by each child process to updated the shared memory
 * based on their PID
 * 
 * @param uniqueId The the unique id of the child provided by the parent process
 * @param sharedMemory The shared memory that the child is going to updated
 */
void ChildProcess(const int uniqueId, int *sharedMemory)
{
    const int processId = getpid();
    const int mailboxIndex = uniqueId + CHILD_ID_MEMORY_OFFSET;

    printf("    Child ID: %d starts\n", uniqueId);

    printf("    Child ID: %d displays shared memory\n", uniqueId);
    printf("        %d\n", sharedMemory[mailboxIndex]);

    printf("    Child ID: %d PID %d\n", uniqueId, processId);

    printf("    Child ID: %d updates shared memory\n", uniqueId);
    sharedMemory[mailboxIndex] = sharedMemory[mailboxIndex] * processId;

    printf("    Child ID: %d displays shared memory\n", uniqueId);
    printf("        %d\n", sharedMemory[mailboxIndex]);

    printf("    Child ID: %d exists with code\n", uniqueId);

    exit(SUCCESS);
}

/**
 * Helper function to wait for each child process to finish, and in the process,
 * store the status and the PID of each child process
 * 
 * @param childProcessesCount The amount of child process that it has to wait
 *                            for
 * @param results The array where each child result will be saved
 */
void CollectChildProcessesResult(
    int childProcessesCount,
    struct ChildProcessResult results[]
)
{
    int currentIndex = 0;
    int curretStatus = 0;

    for(
        currentIndex = 0;
        currentIndex < childProcessesCount;
        currentIndex++
    )
    {
        int curretStatus = SUCCESS;
        struct ChildProcessResult currentResult;

        currentResult.PID = wait(&curretStatus);
        currentResult.status = curretStatus;

        results[currentIndex] = currentResult;
    }
}

/**
 * Helper function to detach the shared memory
 * 
 * @param sharedMemory The shared memory pointer to be detached
 */
void DetachShredMemory(int *sharedMemory)
{
    shmdt(sharedMemory);
}

/**
 * Helper function to extract integer values from console arguments, ignoring 
 * the first argument because it will be the name of the executable
 * 
 * @param realArgc The exact amount of arguments passed through the console
 *                 without including the executable name
 * @param integersString The array of strings received from the console
 * @param integersArray The array of integers where the parsed results are going
 *                      to be placed
 * 
 * @return Whether or not it could convert all arguments into integer values
 */
bool ExtractIntegersFromConsoleString(
    int realArgc,
    char **integersString,
    int integersArray[]
)
{
    int currentIndex = 0;
    int currentValue = 0;
    int currentStringIndex = 0;

    for(currentIndex = 0; currentIndex < realArgc; currentIndex++)
    {
        currentStringIndex = currentIndex + CHILD_INDEX_TO_ID_OFFSET;

        if(StringIsNumber(integersString[currentStringIndex]) == false)
        {
            return false;
        }

        if(
            sscanf(integersString[currentStringIndex], "%d", &currentValue) ==
            NUMBER_SCANNED_CORRECTLY
        )
        {
            integersArray[currentIndex] = currentValue;
        }
        else
        {
            return false;
        }
    }

    return true;
}

/**
 * Helper function to fork the needed child process
 * 
 * @param childProcessesCount The amount of child process needed to be forked
 * 
 * @retrun Whether or not it could fork all the childs specified
 */
bool ForkChildProcesses(int childProcessesCount, int *sharedMemory)
{
    int currentIndex = 0;
    int currentChildUniqueId = 0;

    for(currentIndex = 0; currentIndex < childProcessesCount; currentIndex++)
    {
        currentChildUniqueId = currentIndex + CHILD_INDEX_TO_ID_OFFSET;

        int forkResult = fork();

        if(forkResult == FORK_CHILD)
        {
            ChildProcess(currentChildUniqueId, sharedMemory);
        }
        else if(forkResult < FORK_CHILD)
        {
            puts(
                "We could not spawn all the child processes specified.\n"
                "Gracefully shutting down the program due to invalid input..."
            );

            return false;
        }
    }

    return true;
}

/**
 * Helper function to obtain the real amount of parameters passed through the
 * console
 * 
 * NOTE: The first argument passed to the program is the name of the executable
 * file. Therefore the real amount of arguments is:
 * Formula: argc - 1
 * 
 * @param argc The tha amount of arguments received from the operating system
 */
int GetRealConsoleArgumentsCount(int argc)
{
    return argc - 1;
}

/**
 * Helper function to obtain the shared memory id and attach it
 * 
 * @param integersCount The amount of integers in the shared memory
 * @param memoryId The memory id pointer where the memory id will be stored
 * @param memory The pointer to shared memory
 * 
 * @return Whether the shared memory was attached or not
 */
bool GetSharedMemory(int integersCount, int *memoryId, int **memory)
{
    puts("Parent: receives shared memory");
    if(GetSharedMemoryId(integersCount, memoryId) == false)
    {
        return false;
    }

    puts("Parent: attaches shared memory");
    if(AttachShredMemory(*memoryId, memory) == false)
    {
        return false;
    }

    return true;
}

/**
 * Helper function to get the id when requesting shared memory from the system
 * 
 * @param integersCount The amount of integers for what the memory is needed
 * @param memoryId An integer pointer where the memory id is going to be stored
 * 
 * @return Whether or not the id was obtained successfully 
 */
bool GetSharedMemoryId(int integersCount, int *memoryId)
{
    *memoryId = shmget(
        IPC_PRIVATE,
        integersCount * sizeof(int),
        IPC_CREAT | 0666
    );

    if (*memoryId < 0) {
        printf(
            "%s",
            "We could not get shared memory from the system.\n\n"
            "Gracefully shutting down the program.\n"
        );
        
        return false;
    }

    return true;
}

/**
 * Helper function to initialize an array of integers to zero
 * 
 * @param size The size of the array
 * @param numbers The array that is going to be initialized
 */
void InitializeIntegerArray(int size, int numbers[])
{
    int index = 0;

    for(index = 0; index < size; index++)
    {
        numbers[index] = 0;
    }
}

/**
 * Helper function to initialize the shared memory with the console arguments
 * provided
 * 
 * @param numberscount The amount of argument provided
 * @param sharedMemory The pointer to the shared memory
 * @param numbers The array of integers containing the values of the console 
 *                arguments
 */
void InitializeSharedMemory(
    const int numberscount,
    int *sharedMemory,
    const int numbers[]
)
{
    int currentIndex = 0;

    for(currentIndex = 0; currentIndex < numberscount; currentIndex++)
    {
        sharedMemory[currentIndex] = numbers[currentIndex];
    }
}

/**
 * Helper function that prints the content of an array containing the results of
 * all the child processes
 * 
 * @param childProcessesCount The amount of child process
 * @param results The array containing the results of the child processes
 */
void PrintChildProcessesResults(
    int childProcessesCount,
    struct ChildProcessResult results[]
)
{
    int currentChildIndex = 0;
    int currentChildUniqueId = 0;

    for(
        currentChildIndex = 0;
        currentChildIndex < childProcessesCount;
        currentChildIndex++
    )
    {
        currentChildUniqueId = currentChildIndex + CHILD_INDEX_TO_ID_OFFSET;

        printf(
            "    Child -> Status: %d | PID: %d\n",
            results[currentChildIndex].status,
            results[currentChildIndex].PID
        );
    }
}

/**
 * Helper function to print a specified part of the shared memory
 * 
 * @param prefix A String to print before printing each shared memory value
 * @param fromIndex The start index from where the shared memory is going to be
 *                  printed
 * @param toIndex The end index to where the shared memory is going to be
 *                printed
 * @param sharedMemory The shared memory to be printed
 */
void PrintSharedMemory(
    char prefix[],
    int fromIndex,
    int toIndex,
    int *sharedMemory
)
{
    int currentIndex = 0;
    int currentMailboxIndex = 0;

    for(currentIndex = fromIndex; currentIndex < toIndex; currentIndex++)
    {
        currentMailboxIndex = currentIndex + CHILD_INDEX_TO_ID_OFFSET;

        printf("%s", prefix);
        printf(
            "Mailbox: %d -> %d\n",
            currentMailboxIndex,
            *(sharedMemory + currentIndex)
        );
    }
}

/**
 * Helper function to remove the shared memory
 * 
 * @param sharedMemoryId The shared memory id to be removed
 */
void RemoveSharedMemory(int sharedMemoryId)
{
    shmctl(sharedMemoryId, IPC_RMID, NULL);
}

/**
 * Helper function to determine if a string is composed of numbers
 * 
 * @param string The string that is going to be checked to detect if it is
 *               composed only by numbers
 * 
 * @return Whether or not the string only contains numbers
 */
bool StringIsNumber(const char string[])
{
    int currentIndex = 0;
    bool endOfStringReached = false;

    while(endOfStringReached == false)
    {
        char currentChar = string[currentIndex];

        if(currentChar == END_OF_STRING)
        {
            endOfStringReached = true;
        }
        else
        {
            if(!isdigit(currentChar))
            {
                return false;
            }
        }

        currentIndex++;
    }

    return true;
}

/**
 * Helper function to validate the amount of arguments received from the console
 * 
 * @param realArgc The amount of arguments received from the console
 * 
 * @return Whether or not the amount of arguments is correct
 */
bool ValidConsoleArgumentsCount(int realArgc)
{
    if(realArgc >= MIN_CONSOLE_ARGUMENTS && realArgc <= MAX_CONSOLE_ARGUMENTS)
    {
        return true;
    }

    return false;
}

/**
 * Helper function to validate values from the console arguments
 * 
 * @param realArgc The amount of arguments received from the console
 * @param args The actual values received from the console
 * 
 * @return Whether or not the input is valid
 */
bool ValidConsoleValues(int realArgc, char **args, int integersArray[])
{
    char shutDownMessage[] = "Gracefully shutting down the program"
                           " due to invalid input...";

    if(ValidConsoleArgumentsCount(realArgc) == false)
    {
        printf(
            "The amount of arguments passed through the console is not valid.\n"
            "You must provide between %d - %d integer arguments (inclusive)\n\n",
            MIN_CONSOLE_ARGUMENTS,
            MAX_CONSOLE_ARGUMENTS
        );

        puts(shutDownMessage);

        return false;
    }

    if(ExtractIntegersFromConsoleString(realArgc, args, integersArray) == false)
    {
        printf(
            "The arguments provided are not valid integers.\n"
            "Each argument must be an integer between %d - %d (inclusive).\n\n",
            MIN_VALUE,
            MAX_VALUE
        );

        puts(shutDownMessage);

        return false;
    }
    
    if(ValidIntegerValuesInArray(realArgc, integersArray) == false)
    {
        printf(
            "The arguments provided are not valid integers.\n"
            "Each argument must be an integer between %d - %d (inclusive).\n\n",
            MIN_VALUE,
            MAX_VALUE
        );

        puts(shutDownMessage);

        return false;
    }

    return true;
}

/**
 * Helper function to validate that each value within an array of integers is
 * between the valid range: MIN_VALUE - MAX_VALUE
 * 
 * @param arraySize The size of the array of integers
 * @param integerArray The actual array containing the integers
 * 
 * @return Whether or not all the values in the array are valid
 */
bool ValidIntegerValuesInArray(int arraySize, int integersArray[])
{
    int currentIndex = 0;

    for(currentIndex = 0; currentIndex < arraySize; currentIndex++)
    {
        if(
            integersArray[currentIndex] < MIN_VALUE || 
            integersArray[currentIndex] > MAX_VALUE
        )
        {
            return false;
        }
    }

    return true;
}